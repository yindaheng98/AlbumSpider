import json
from datetime import datetime

from .common import datetime_format


class DownloadInfo:
    """
    一个记录下载信息的类
    记录应该从何处下载元数据和内容以及上次更新的时间
    """

    def __init__(self,
                 driver_type_name: str = '想使用哪种驱动进行操作？在此填上驱动名称',
                 download_info: str or dict = '想给进行操作的驱动传递什么变量？在此填上变量，最好是dict或str',
                 last_updated_time: datetime = None):
        """
        初始化AlbumDownloadInfo
        :param driver_type_name: 这个DownloadInfo应该给哪个driver进行处理
        :param download_info: 传递给driver的下载信息
        :param last_updated_time: 上次更新的时间
        """
        self.driver_type_name = driver_type_name
        self.download_info = download_info
        self.last_updated_time = last_updated_time

    def serialize(self) -> dict:
        """
        将DownloadInfo组织为一个dict
        :return: DownloadInfo对应的dict
        """
        data = {
            'driver_type_name': self.driver_type_name,
            'download_info': self.download_info
        }
        if self.last_updated_time is not None:
            data['last_updated_time'] = self.last_updated_time.strftime(datetime_format)
        return data

    def deserialize(self, data: dict):
        self.driver_type_name = data['driver_type_name']
        self.download_info = data['download_info']
        if 'last_updated_time' in data:
            self.last_updated_time = datetime.strptime(data['last_updated_time'], datetime_format)
        return data

    def __str__(self):
        return json.dumps(self.serialize())
