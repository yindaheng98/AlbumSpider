# AlbumSpider

自动爬取网络上的图片集并将其与元数据放在一起。

## MetaSpiderDriver

一个抽象类，用于元数据的获取。

主要包含一个输入任意合法的网页URL，输出JSON格式元数据的抽象方法。
你需要在其中实现从URL上爬取元数据，组织为JSON输出的过程。

## MetaGetter

一个添加Driver方法用于向

## AlbumSpiderDriver

内容获取器驱动抽象类，用于获取指定的内容

主要包含一个输入任意合法的网页URL和一个文件操作器，没有返回的方法。
你需要在其中实现从URL上获取内容，使用文件操作器写入到指定位置的过程。